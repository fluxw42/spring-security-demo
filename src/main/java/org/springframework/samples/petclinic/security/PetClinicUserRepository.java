package org.springframework.samples.petclinic.security;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PetClinicUserRepository extends CrudRepository<PetClinicUser, Long> {

    Optional<PetClinicUser> findByUsername(String userName);

}
