package org.springframework.samples.petclinic.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DatabaseUserDetailsService implements UserDetailsService, UserDetailsPasswordService {

    private final PetClinicUserRepository userRepository;

    public DatabaseUserDetailsService(final PetClinicUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final PetClinicUser user = this.userRepository.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("Could not find user with name [" + username + "]"));

        final List<GrantedAuthority> userRoles = user.getRoles()
            .stream()
            .map(r -> new SimpleGrantedAuthority("ROLE_" + r.getName().toUpperCase()))
            .collect(Collectors.toList());

        return new User(user.getUsername(), user.getPassword(), userRoles);
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        final PetClinicUser petClinicUser = this.userRepository.findByUsername(user.getUsername())
            .orElseThrow(() -> new IllegalArgumentException("Could not update password, user not found [" + user.getUsername() + "]"));

        petClinicUser.setPassword(newPassword);
        this.userRepository.save(petClinicUser);

        final List<GrantedAuthority> userRoles = petClinicUser.getRoles()
            .stream()
            .map(r -> new SimpleGrantedAuthority("ROLE_" + r.getName().toUpperCase()))
            .collect(Collectors.toList());

        return new User(user.getUsername(), user.getPassword(), userRoles);
    }
}
