package org.springframework.samples.petclinic.security;

import org.springframework.samples.petclinic.model.NamedEntity;

import javax.persistence.Entity;

@Entity
public class Role extends NamedEntity {

    public Role() {
        super();
    }

    public Role(final String role) {
        setName(role);
    }

}
