package org.springframework.samples.petclinic.security;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class PetClinicUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;

}
