package org.springframework.samples.petclinic.owner;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PetTypeRepository extends CrudRepository<PetType, Integer> {

    Optional<PetType> findByName(final String name);

}
