package org.springframework.samples.petclinic.vet;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SpecialtyRepository extends CrudRepository<Specialty, Integer> {

    /**
     * Retrieve all {@link Specialty} values from the data store.
     *
     * @return a Collection of {@link Specialty}.
     */
    @Query("SELECT specialty FROM Specialty specialty")
    @Transactional(readOnly = true)
    List<Specialty> findAllSpecialties();

}
