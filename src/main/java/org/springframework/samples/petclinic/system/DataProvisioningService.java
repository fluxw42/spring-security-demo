package org.springframework.samples.petclinic.system;

import org.springframework.samples.petclinic.owner.*;
import org.springframework.samples.petclinic.security.PetClinicUser;
import org.springframework.samples.petclinic.security.PetClinicUserRepository;
import org.springframework.samples.petclinic.security.Role;
import org.springframework.samples.petclinic.security.RoleRepository;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.SpecialtyRepository;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.*;

@Service
public class DataProvisioningService {

    /**
     * Random number generator, used to select random values to populate the database
     */
    private static final Random RANDOM = new Random();

    /**
     * All possible pet names
     */
    private static final String[] PET_NAMES = new String[]{
        "Fusion", "Doll", "Brady", "Toto", "Kelso", "Nosferatu", "Einstein", "Bell", "Pookie", "Nastradamus", "Bingo",
        "Bones", "Bo", "Galiga", "Benson", "Savant", "Fuzzbucket", "Atlas", "Dewey", "Abby", "Galaga", "Matlock",
        "Caper", "Rafiki", "Checker", "Mathis", "Dewey", "Vanilla", "Goofy", "Saki", "Rex", "Fluffy", "Ranger", "Fifi"
    };

    /**
     * The full set of specialties for a vet
     */
    private static final String[] SPECIALTIES = new String[]{
        "radiology", "dentistry", "surgery"
    };

    /**
     * All different pet types
     */
    private static final String[] PET_TYPES = new String[]{
        "cat", "dog", "lizard", "snake", "bird", "hamster", "horse", "turtle", "fish", "rabbit", "chicken", "goat"
    };

    /**
     * An array of first names, used to populate database fields where a first name is required.
     */
    private static final String[] FIRST_NAMES = new String[]{
        "Irina", "Lachelle", "Julieann", "Edmundo", "Kellye", "Kandace", "Michel", "Rufus", "Jimmie", "Yasmine",
        "Antione", "Hiedi", "Aretha", "Rae", "Shantell", "Patrica", "Ayesha", "Coralie", "Teressa", "Isabelle",
        "Latoria", "Dominique", "Rogelio", "Birgit", "Lavon", "Bok", "Viva", "Velvet", "Ericka", "Sulema"
    };

    /**
     * An array of last names, used to populate database fields where a last name is required.
     */
    private static final String[] LAST_NAMES = new String[]{
        "Imai", "Lovern", "Joye", "Eason", "Karns", "Kuss", "Mcknight", "Rosso", "Josephson", "Yingst", "Avendano",
        "Hennigan", "Auxier", "Racette", "Salser", "Propst", "Appling", "Creekmore", "Twitchell", "Iacovelli",
        "Loisel", "Drew", "Reiser", "Baskerville", "Lawrence", "Bolander", "Vanloan", "Varano", "Eagar", "Shirley"
    };

    /**
     * An array of cities, used to populate database fields where a city or address is required.
     */
    private static final String[] CITY_NAME = new String[]{
        "Linston", "Stoneton", "Brightford", "Wellland", "Newcastle", "Ironwell", "Southwood", "Wheatshore", "Greenflower",
        "Witchshore", "Newlake", "Winterwell", "Windhollow", "Waterpond", "Merrimill", "Riverbridge", "Eastsea", "Ordale",
        "Greyholt", "Beachcoast", "Bellake", "Millmeadow", "Westerbarrow", "Strongspring", "Summerhaven", "Swynham",
        "Springwolf", "Aldwynne", "Summerriver", "Deepcastle", "Woodwinter", "Fieldnesse", "Clearden", "Marblesilver",
        "Aldfair", "Bluewall", "Verttown", "Aldbourne", "Winterway", "Merricliff", "Faybay", "Redcrest", "Coldcastle",
        "Fogedge", "Greymallow", "Goldhedge", "Baypond", "Brightwell", "Brightgate", "Lochwynne"
    };

    /**
     * Different types of streets, used to generate addresses (street names)
     */
    private static final String[] STREET_TYPE = new String[]{
        "lane", "road", "square", "street", "avenue", "terrace", "way"
    };

    /**
     * A list of possible vet visit descriptions
     */
    private static final String[] VISIT_DESCRIPTION = new String[]{
        "Got neutered", "Routine checkup", "Spayed", "Rabies shot", "Chipped", "Collect medicines"
    };

    /**
     * The number of random owners to generate
     */
    private static final int OWNER_COUNT = 30;

    /**
     * The number of random vets to generate
     */
    private static final int VET_COUNT = 10;

    private final OwnerRepository ownerRepo;
    private final PetRepository petRepo;
    private final PetTypeRepository petTypeRepo;
    private final SpecialtyRepository specialtyRepo;
    private final VetRepository vetRepo;
    private final PetClinicUserRepository userRepo;
    private final RoleRepository roleRepo;

    public DataProvisioningService(final OwnerRepository ownerRepo, final PetRepository petRepo, final PetTypeRepository petTypeRepo,
                                   final SpecialtyRepository specialtyRepo, final VetRepository vetRepo,
                                   final PetClinicUserRepository userRepo, final RoleRepository roleRepo) {
        this.ownerRepo = ownerRepo;
        this.petRepo = petRepo;
        this.petTypeRepo = petTypeRepo;
        this.specialtyRepo = specialtyRepo;
        this.vetRepo = vetRepo;
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
    }

    @PostConstruct
    public void provisionData() {

        createUsers();

        // Create pet types
        for (final String name : PET_TYPES) {
            final PetType petType = new PetType();
            petType.setName(name);
            this.petTypeRepo.save(petType);
        }

        // Generate owners with at least one pet
        for (int i = 0; i < OWNER_COUNT; i++) {
            final Owner owner = new Owner();
            owner.setFirstName(getRandomEntry(FIRST_NAMES));
            owner.setLastName(getRandomEntry(LAST_NAMES));
            owner.setAddress(generateRandomAddress());
            owner.setCity(getRandomEntry(CITY_NAME));
            owner.setTelephone(generateRandomPhoneNumber());

            final int petCount = RANDOM.nextInt(3) + 1;
            for (int j = 0; j < petCount; j++) {
                owner.addPet(generateRandomPet());
            }

            this.ownerRepo.save(owner);
        }

        // Create specialties
        for (final String name : SPECIALTIES) {
            final Specialty specialty = new Specialty();
            specialty.setName(name);
            this.specialtyRepo.save(specialty);
        }


        // Generate vets with no or a single specialty
        for (int i = 0; i < VET_COUNT; i++) {

            final Vet vet = new Vet();
            vet.setFirstName(getRandomEntry(FIRST_NAMES));
            vet.setLastName(getRandomEntry(LAST_NAMES));

            if (RANDOM.nextBoolean()) {
                vet.addSpecialty(getRandomEntry(this.specialtyRepo.findAllSpecialties()));
            }

            this.vetRepo.save(vet);
        }

        // Create visits for each pet
        for (final Pet pet : this.petRepo.findAll()) {
            final int visitCount = RANDOM.nextInt(5);
            for (int i = 0; i < visitCount; i++) {
                final Visit visit = new Visit();
                visit.setDate(generateRandomDate());
                visit.setDescription(getRandomEntry(VISIT_DESCRIPTION));
                pet.addVisit(visit);
            }
            this.petRepo.save(pet);
        }

    }

    /**
     * Create 2 users with the correct role
     */
    private void createUsers() {
        final Role adminRole = roleRepo.save(new Role("admin"));
        final Role userRole = roleRepo.save(new Role("user"));

        final PetClinicUser adminUser = new PetClinicUser();
        adminUser.setUsername("admin");
        adminUser.setPassword("{noop}admin123");
        adminUser.setRoles(new HashSet<>(Arrays.asList(adminRole, userRole)));
        this.userRepo.save(adminUser);

        final PetClinicUser regularUser = new PetClinicUser();
        regularUser.setUsername("user");
        regularUser.setPassword("{noop}user123");
        regularUser.setRoles(Collections.singleton(userRole));
        this.userRepo.save(regularUser);

    }

    /**
     * Generate a random pet
     *
     * @return The new (unsaved) pet
     */
    private Pet generateRandomPet() {
        final Pet pet = new Pet();
        pet.setType(getRandomEntry(petRepo.findPetTypes()));
        pet.setBirthDate(generateRandomDate());
        pet.setName(getRandomEntry(PET_NAMES));
        return pet;
    }

    /**
     * Generate a random date
     *
     * @return A random date
     */
    private static LocalDate generateRandomDate() {
        final int year = RANDOM.nextInt(9) + 2010;
        final int dayOfYear = RANDOM.nextInt(365) + 1;
        return LocalDate.ofYearDay(year, dayOfYear);
    }

    /**
     * Generate a random phone number
     *
     * @return The new random phone number
     */
    private static String generateRandomPhoneNumber() {
        final StringBuilder sb = new StringBuilder("0");
        for (int i = 0; i < 9; i++) {
            sb.append(RANDOM.nextInt(10));
        }
        return sb.toString();
    }

    /**
     * Pick a random entry from the given array of values
     *
     * @param values The array to pick from
     * @return The random entry or 'null' when the given array is null or empty
     */
    private static String getRandomEntry(final String[] values) {
        if (values != null && values.length > 0) {
            return values[RANDOM.nextInt(values.length)];
        }
        return null;
    }

    /**
     * Pick a random entry from the given iterable of values
     *
     * @param iterable The iterable values
     * @param <T>      The object type
     * @return The random entry or 'null' when the given iterable is null or empty
     */
    private static <T> T getRandomEntry(final Iterable<T> iterable) {
        if (iterable == null) {
            return null;
        }

        final List<T> values = new ArrayList<T>();
        iterable.forEach(values::add);

        if (values.isEmpty()) {
            return null;
        }

        return values.get(RANDOM.nextInt(values.size()));
    }

    /**
     * Generate a random address (street name + number)
     *
     * @return The address
     */
    private static String generateRandomAddress() {
        return String.format(
            "%s %s %d",
            getRandomEntry(CITY_NAME),
            getRandomEntry(STREET_TYPE),
            RANDOM.nextInt(100)
        );
    }

}
