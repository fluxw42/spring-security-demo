/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.system;


import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
class WelcomeController {

    @GetMapping("/")
    public String welcome(Model model, Authentication authentication) {

        if (authentication instanceof OAuth2AuthenticationToken) {
            welcomeOAuth2User(model, (OAuth2AuthenticationToken) authentication);
        } else if (authentication instanceof UsernamePasswordAuthenticationToken) {
            welcomeLocalUser(model, (UsernamePasswordAuthenticationToken) authentication);
        }

        return "welcome";
    }

    /**
     * Add the local username to the model
     *
     * @param model The model
     * @param token The authentication token
     */
    private void welcomeLocalUser(final Model model, final UsernamePasswordAuthenticationToken token) {
        model.addAttribute("username", token.getName());
    }

    /**
     * Add the OAuth2 username and profile picture to the model
     *
     * @param model The model
     * @param token The authentication token
     */
    private void welcomeOAuth2User(Model model, OAuth2AuthenticationToken token) {
        final OAuth2User principal = token.getPrincipal();
        final Map<String, Object> attributes = principal.getAttributes();
        model.addAttribute("username", attributes.get("name"));
        model.addAttribute("picture", attributes.get("picture"));
    }
}
